# pw2.py
#
# A weeWX driver for the Tesla Powerwall 2.
#
# Copyright (C) 2018 Daniel Kjellin                info<at>daik.se
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see http://www.gnu.org/licenses/.
#
# Version: 0.1                                      Date: 13 June 2019
#
# Revision History
#   13 June 2019      v0.1    - initial release
#
""" A weeWX driver for Tesla Powerwall 2.

The driver communicates directly with the powerwall 2 without the need for any
other application. The driver produces loop packets that may be used with a
custom weeWX schema to produce archive records may be stored and processed by
the weeWX engine.
"""
import socket
import syslog

import time
import urllib2 #should realy upgrade to requests
import ssl
import json
from datetime import datetime
import iso8601

import weewx.drivers
import weewx
import weewx.units

DRIVER_NAME = 'PW2'
DRIVER_VERSION = '0.1'


def loader(config_dict, engine):  # @UnusedVariable
    return PW2Driver(config_dict[DRIVER_NAME])

def confeditor_loader():
    return PW2ConfigurationEditor()

# ============================================================================
#                            class PW2Driver
# ============================================================================

class PW2Driver(weewx.drivers.AbstractDevice):
    """Class representing connection to the Powerwall 2."""

    host = ""
    skipOldValues = False
    waitTime = 60

    def __init__(self, pw2_dict):
        """Initialise an object of type PW2."""
        # model
        try:
            self.host = pw2_dict.get('host')

        except KeyError:
            raise Exception("Required parameter 'host' was not specified.")

        logdbg('%s driver version is %s' % (DRIVER_NAME, DRIVER_VERSION))
        self.skipOldValues = pw2_dict.get("skipOld", True)
        self.waitTime = int(pw2_dict.get("waitTime", 60))
        weewx.units.obs_group_dict['pwHomeLoad'] = 'group_power'
        weewx.units.obs_group_dict['pwBatteryLoad'] = 'group_power'
        weewx.units.obs_group_dict['pwSolarProd'] = 'group_power'
        weewx.units.obs_group_dict['pwGridLoad'] = 'group_power'
        weewx.units.obs_group_dict['pwSoc'] = 'group_percent'


        weewx.units.conversionDict['watt'] = {'kwatt': lambda x: x / 1000.0,
                                              'Mwatt': lambda x: x / 1000000.0}
        weewx.units.conversionDict['kwatt'] = {'watt': lambda x: x * 1000.0,
                                               'Mwatt': lambda x: x / 1000.0}
        weewx.units.conversionDict['Mwatt'] = {'watt': lambda x: x * 1000000.0,
                                               'kwatt': lambda x: x * 1000.0}
        weewx.units.conversionDict['watt_hour'] = {'kwatt_hour': lambda x: x / 1000.0,
                                                   'Mwatt_hour': lambda x: x / 1000000.0}
        weewx.units.conversionDict['kwatt_hour'] = {'watt_hour': lambda x: x * 1000.0,
                                                    'Mwatt_hour': lambda x: x / 1000.0}
        weewx.units.conversionDict['Mwatt_hour'] = {'watt_hour': lambda x: x * 1000000.0,
                                                    'kwatt_hour': lambda x: x * 1000.0}

    def genLoopPackets(self):
        """Generate packages to be ingested by Weewx.
            This function does not provide any way to
            "catch up" on old data, it will only return the most
            current data from the inverter.
        """
        while True:
            loginf("Sleeping for %s" % self.waitTime)
            time.sleep(self.waitTime)

            yield self.getPacket()

    def getPacket(self):
        """Get a packet from the Powerwall and return it in the expected format """
        loginf("Getting packet")
        stateOfCharge = self.makeRequest("/api/system_status/soe") #percentage
        siteAggregaes = self.makeRequest("/api/meters/aggregates")

        packet = self.extractStateOfCharge(stateOfCharge)
        packet = self.extractAggregateValue(packet, siteAggregaes, "load", "pwHomeLoad")
        packet = self.extractAggregateValue(packet, siteAggregaes, "battery", "pwBatteryLoad")
        packet = self.extractAggregateValue(packet, siteAggregaes, "solar", "pwSolarProd")
        packet = self.extractAggregateValue(packet, siteAggregaes, "site", "pwGridLoad")
        packet['usUnits'] = weewx.METRIC
        packet['dateTime'] = int(time.time())
        logdbg(packet)
        return packet

    def extractStateOfCharge(self, stateOfCharge):
        if 'percentage' in stateOfCharge:
            return {
                "pwSoc" : stateOfCharge['percentage']
            }
        return {}

    def extractAggregateValue(self, packet, siteAggregates, sourceGroup, targetKey):
        if sourceGroup in siteAggregates:
            load = siteAggregates[sourceGroup]
            lastDate = iso8601.parse_date(load.get('last_communication_time', '2019-06-12T19:14:24.790109692+10:00'))
            if (int(time.time()) - int(lastDate.strftime("%s")) > 60*60*1000) and not self.skipOldValues: # more than ten minutes delay, skip if configured to
                return packet
            else:
                val = load.get("instant_power", 0)
                if int(val) > 60:
                    packet[targetKey] = load.get("instant_power", 0)
                else:
                    packet[targetKey] = 0
        return packet



    def makeRequest(self, path):
        """
        Makes the request to the controller card and parses the response and returns it as a dict
        :param path: path segment, this is everything after the port number in the url.
        :return: a dict parsed from the response from the card or an empty dict if server did not respond

        """
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        logdbg("Making request to %s" %path)
        try:
            json_response = urllib2.urlopen(self.host + path, timeout=10, context=ctx)
            resp = json_response.read().decode('utf-8', 'ignore').encode("utf-8")
            return json.loads(resp)
        except urllib2.URLError, e:
            logdbg("Server not responding: %s" %e)
        except ValueError, e:
            logerr("Server responded with invalid data: %s" %e)
        except socket.error, e:
            logerr("Socket error: %s" % e)

        return {}





def logmsg(level, msg):
    syslog.syslog(level, 'pw2: %s' % msg)


def logdbg(msg):
    logmsg(syslog.LOG_DEBUG, msg)


def logdbg2(msg):
    if weewx.debug >= 2:
        logmsg(syslog.LOG_DEBUG, msg)


def loginf(msg):
    logmsg(syslog.LOG_INFO, msg)


def logerr(msg):
    logmsg(syslog.LOG_ERR, msg)

class PW2ConfigurationEditor(weewx.drivers.AbstractConfEditor):
    @property
    def default_stanza(self):
        return """
[PW2]
    # This section is for the Tesla Powerwall 2 driver

    # Hostname or IP address of the Powerwall
    host = http://0.0.0.0

    # The driver to use:
    driver = user.pw2
"""

    def prompt_for_settings(self):
        print "Specify the hostname or address of the Envoy"
        host = self._prompt('host', 'http://0.0.0.0')
        return {'host': host}
