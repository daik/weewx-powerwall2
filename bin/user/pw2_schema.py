# pw2_schema.py
#
# A weeWX schema for use with the weewx-pw2 driver and a Tesla Powerwall 2.
#
# Copyright (C) 2019 Daniel Kjellin                         info<at>daik.se
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see http://www.gnu.org/licenses/.
#
# Version: 1.0.0                                      Date: 12 January 2018
#
# Revision History
#  13 June     v1.0.0      - initial release
#
"""The weewx-pw2 schema"""
# ============================================================================
# This file contains the default schema of the weewx-pw2 archive table. It
# is only used for initialization, or in conjunction with the weewx_config
# utility --create-database and --reconfigure options. Otherwise, once the
# tables are created the schema is obtained dynamically from the database.
#
# The schema may be trimmed of any unused types if required, but it will not
# result in much space being saved as most of the space is taken up by the
# primary key indexes (type "dateTime").
# ============================================================================
#
PW2_SCHEMA_VERSION = '1.0.0'

# Define schema for archive table
pw2_schema = [
    ('dateTime',       'INTEGER NOT NULL UNIQUE PRIMARY KEY'),
    ('usUnits',        'INTEGER NOT NULL'),
    ('interval',       'INTEGER NOT NULL'),
    ('pwSoc',          'REAL'),
    ('pwHomeLoad',     'REAL'),
    ('pwBatteryLoad',  'REAL'),
    ('pwSolarProd',    'REAL'),
    ('pwGridLoad',     'REAL'),
    ]
