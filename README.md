# Tesla Powerwall 2 Weewx extension #

Using [weeWX](http://weewx.com/ "WeeWX - Open source software for your weather station") to record power generation and consumption data from a Tesla Powerwall 2

## Description ##

The *PW2* extension allows weeWX to download, record and report power data from a Tesla Powerwall 2.

The *PW2* extension consists of:
- a weeWX driver for Tesla Powerwall 2,
- a custom weeWX database schema to support the Powerwall 2,


## Pre-Requisites ##

The *PW2* extension requires weeWX v3.7.0 or greater.

The *PW2* extension requires a functioning TCP/IP communications link between the weeWX machine and the Powerwall 2. In my case, this was achieved using a Raspberry Pi 2 model B+running Raspbian Jessie connected to a network that has a wired connection to the Powerwall 2. This link functioned without the need for installation of any additional software other than the *PW2* extension.

## Installation ##

The *PW2* extension can be installed manually or automatically using the *wee_extension* utility. The preferred method of installation is through the use of *wee_extension*.

**Note:** Symbolic names are used below to refer to some file location on the weeWX system. These symbolic names allow a common name to be used to refer to a directory that may be different from system to system. The following symbolic names are used below:

-   *$DOWNLOAD_ROOT*. The path to the directory containing the downloaded *PW2* extension.
-   *$HTML_ROOT*. The path to the directory where weeWX generated reports are saved. This directory is normally set in the *[StdReport]* section of *weewx.conf*. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.
-   *$BIN_ROOT*. The path to the directory where weeWX executables are located. This directory varies depending on weeWX installation method. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.
-   *$SKIN_ROOT*. The path to the directory where weeWX skin folders are located This directory is normally set in the *[StdReport]* section of *weewx.conf*. Refer to [where to find things](http://weewx.com/docs/usersguide.htm#Where_to_find_things "where to find things") in the weeWX [User's Guide](http://weewx.com/docs/usersguide.htm "User's Guide to the weeWX Weather System") for further information.

### Installation using the wee_extension utility ###

1.  Download the latest *PW2* extension from the *PW2* [extension releases page](https://bitbucket.org/daik/weewx-powerwall2/downloads/) into a directory accessible from the weeWX machine.
        
        wget -P $DOWNLOAD_ROOT https://XXXX/XXXX.tar.gz

    where $DOWNLOAD_ROOT is the path to the directory where the *PW2* extension is to be downloaded.

1.  Stop weeWX:

        sudo /etc/init.d/weewx stop

    or

        sudo service weewx stop

1.  Install the *PW2* extension downloaded at step 1 using the *wee_extension* utility:

        wee_extension --install=$DOWNLOAD_ROOT/weewx-pw2-0.1.0.tar.gz

    This will result in output similar to the following:

        Request to install '/var/tmp/weewx-pw2-0.1.0.tar.gz'
        Extracting from tar archive /var/tmp/weewx-pw2-0.1.0.tar.gz
        Saving installer file to /home/weewx/bin/user/installer/PW2
        Saved configuration dictionary. Backup copy at /home/weewx/weewx.conf.20190215124410
        Finished installing extension '/var/tmp/weewx-pw2-0.1.0.tar.gz'

        
1. Configure weewx
        `wee_config --reconfigure`

1. Add schema to binding
Find the [[DataBindings]] section and add schema = user.pw2_schema.pw2_schema to the binding that you plan to use for the *PW2* driver.

        ``` 
        [DataBindings]
         [[wx_binding]]
		database = archive_sqlite
		table_name = archive
		manager = weewx.wxmanager.WXDaySummaryManager
		schema = user.pw2_schema.pw2_schema
		```

1.  Save *weewx.conf*.
 
1.  Start weeWX:

        sudo /etc/init.d/weewx start

    or

        sudo service weewx start

The weeWX log should be monitored to verify data is being read from the Powerwall 2. Setting *debug = 1* or *debug = 2* in *weewx.conf* will provide additional information in the log. Using *debug = 2* will generate significant amounts of log output and should only be used for verification of operation or testing.


## Support ##

General support issues may be raised in the Google Groups [weewx-user forum](https://groups.google.com/group/weewx-user "Google Groups weewx-user forum"). Specific bugs in the *PW2* extension code should be the subject of a new issue raised via the *PW2* extension [Issues Page](https://bitbucket.org/daik/weewx-powerwall2/issues?status=new&status=open).
 
## Licensing ##

The *PW2* extension is licensed under the [GNU Public License v3](https://bitbucket.org/daik/weewx-pw2/src/7c030b9a685ea29b7d1c29b049e43086b60fa682/LICENSE?at=master).
