#!/bin/bash
VERSION=0.1
FILE_NAME=weewx-pw2-$VERSION.tar.gz
rm -f $FILE_NAME
rm -rf build
mkdir -p build/weewx-pw2
cp -R bin skins changelog install.py LICENSE README.md weewx.conf build/weewx-pw2
cd build
tar -zcvf ../$FILE_NAME weewx-pw2
cd ..
