#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
#                     Installer for Powerwall 2 weewx extension
#
# Version: 0.0.1                                        Date: 13 June 2019
#
# Revision History
#  13 June 2019    v0.0.1
#       - initial implementation
#
import weewx

from distutils.version import StrictVersion
from setup import ExtensionInstaller

REQUIRED_VERSION = "3.7.0"
PW2_DRIVER_VERSION = "0.1"

def loader():
    return PW2Installer()


class PW2Installer(ExtensionInstaller):
    def __init__(self):
        if StrictVersion(weewx.__version__) < StrictVersion(REQUIRED_VERSION):
            msg = "%s requires weeWX %s or greater, found %s" % ('PW2 ' + PW2_DRIVER_VERSION,
                                                                 REQUIRED_VERSION,
                                                                 weewx.__version__)
            raise weewx.UnsupportedFeature(msg)
        super(PW2Installer, self).__init__(
                version=PW2_DRIVER_VERSION,
                name='PW2',
                description='WeeWX support for recording solar PV power generation, state of charge and consumption data from a Tesla Powerwall 2.',
                author="Daniel Kjellin",
                author_email="info@daik.se",
                restful_services=[],
                config={
                    'PW2': {
                        'host': 'replace_me',
                        'driver': 'user.pw2',
                    }
                },
                files=[
                    ('bin/user', ['bin/user/pw2_schema.py']),
                    ('bin/user', ['bin/user/iso8601.py']),
                    ('bin/user', ['bin/user/pw2.py'])
                ]
            )

